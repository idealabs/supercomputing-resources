

# BYU Supercomputer

- [BYU Supercomputer](https://rc.byu.edu/)
  - **Create an account here:** [Create account](https://rc.byu.edu/account/create/)
    - Need an advisor to approve i.e David or Sean.

# BYU Supercomputing Resources

## Basic SSH Connection

Connect to the supercomputer:

```bash
ssh username@ssh.rc.byu.edu
```

## Common Commands and Headers

- `sbatch`: Run a job. Usage: `sbatch [headers] [script_to_run.sh]`. Outputs a job number.
- `scontrol show job [jobid]`: Show information about a job.
- `squeue -u [netid]`: Show current jobs running under a specified username.
- `squeue --me`: Shows current jobs running that are tied to your account.
- `scancel [jobid]`: Cancel a specific job.
- `vim slurm-[jobid].out`: Open the file containing the output of a job.

## Creating a job script
- The easiest way to create a job script is by using the job script generator provided by the supercomputing deparment found [here](https://rc.byu.edu/documentation/slurm/script-generator).

## Common Headers Explained

- `-H`: Held state; the job will not run.
- `-N1`: Number of nodes (servers), in this case, 1.
- `-n1`: Number of tasks (CPUs), in this case, 1.
- `--mem-per-cpu=64G`: Memory allocation per CPU (RAM), here it's 64G.
- `--qos=test`: Assign to the test queue, which has limited computational resources.

## Environment and Modules

Note that by default many modules are not loaded such as cuda and miniconda which are necessary for many computing tasks. Adding a module is as follows.

1. See available modules: `module available`
2. Find necessary module and version i.e `miniconda/latest`
3. Load the module: `module load miniconda/latest`
4. Save the module to default so you don't have to load it every time you log in: `module save`
5. Check your default to see which modules you have loaded `module list`

- `export PATH="~/bin:$PATH"`: Add bin files to the path for command-line accessibility.
- `module available`: List all available modules.
- `module load [module/version]`: Load a specific module.
- `module list`: List currently loaded modules.
- `module unload [module]`: Unload a specific module.
- `module purge`: Unload all modules.

## File Transfer

- From supercomputer to local: `scp username@ssh.rc.byu.edu:/home/netid/myfile.txt Documents/`
- From local to supercomputer: `scp Documents/myfile.txt username@ssh.rc.byu.edu:/home/netid/`

## Sample Command Line Usage

```bash
sbatch -N1 -n1 --mem-per-cpu=100M -t00:05:00 --qos=test hostname.sh
```

## Bash Script File (.sh) Example

```bash
#!/bin/bash
#***** NOTE: run this using: sg grp_supplychainai "sbatch thefilename"

#SBATCH --time=48:00:00   # walltime
#SBATCH --ntasks=1   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --gpus=1
#SBATCH -C 'pascal'   # features syntax (use quotes): -C 'a&b&c&d'
#SBATCH --mem-per-cpu=32768M   # memory per CPU core
#SBATCH -J "seal_test"   # job name

# Set the max number of threads to use for programs using OpenMP. Should be <= ppn. Does nothing if the program doesn't use OpenMP.
export OMP_NUM_THREADS=$SLURM_CPUS_ON_NODE

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE

python ./my_script.py

```

## Additional Features

- Specific QOS: Use `#qos=cs` for CS computers, `#SBATCH -C pascal` for requesting an P100.
- Mamba Environment: Use `#!/bin/bash --login` and `mamba activate env`.

## Group File Sharing

- File Sharing Groups: Can be created for collaboration. see [here](https://rc.byu.edu/wiki/?id=Group+File+Sharing)
- When running scripts that you want the whole group to be able to see, use the following format
`sg grp_groupname "sbatch script_to_run.sh"`

## Real-Time Stats and Results

Doing the following will allow you to ssh into a compute node and utilize it from the command line instead of submitting a job.

- `salloc`: Use with `sbatch` parameters for real-time resource usage.
- `chmod +w filename.sh`: Update permissions for a script.
- `./[script]`: Run the program as a script.
- `ssh node_name`: SSH onto a node from another terminal.
- `htop -u netid`: Shows CPU utilization and memory.
- `nvidia-smi`: Displays GPU utilization.
- example: `salloc --time="48:00:00 gpus=1 nodes=1 ntasks=1 -C 'pascal'`
- The above creates an instance that will run for 48 hrs, has a single pascal (P100) gpu, 1 node (server), and one task (core).

## GUI Setup

- Visit: [vis.rc.byu.edu](https://vis.rc.byu.edu)
- Choose an environment (e.g., cinnamon).
- Open terminal.
- Use `salloc` with `sbatch` parameters.
- Run the script with `./[script name]`.

## Setting up Jupyter Notebooks

### First Terminal:

- SSH into supercomputer: `ssh user@ssh.rc.byu.edu`.
- SSH into a specific node: `salloc --mem=16G --time=5:00:00`.
- Activate JupyterLab environment: `mamba activate jupyterlab`.
- Start JupyterLab: `jupyter-lab --no-browser`.

### Second Terminal:

- SSH into the Jupyter notebook with a port: `ssh -N -J [user]@ssh.rc.byu.edu -L 8888:localhost:8888 [user]@[node]`
  Replace placeholders with appropriate values (username, Jupyter port, and node ID).
- Paste the URL from the first terminal into a browser.

# Other Supercomputing Resources

# NSF Access

- [NSF Access](https://access-ci.org/)
  - **Register for access at this link:** [Register for access](https://registry.access-ci.org/registry/co_petitions/petitionerAttributes/213231/token:0d68d9f068c6b68458972f3785c27a22d0bcbfba)
    - You will create a password and receive a username/access id in your inbox.
  - **Getting Allocated Resources:**
    - Information on the resource types and their requirements can be found [here](https://allocations.access-ci.org/project-types).
    - Utilize a resource credit system that you can use for resources. More resources require more requirements like 3-10 page proposals.
    - You need to submit a request for resources done by logging into your account -> allocations -> request new project.

# NAIR Pilot

- [NAIR Pilot](https://nairrpilot.org/allocations)
  - **Requires proposal submission:**
    - Limited to the following fields:
      - Testing, evaluating, verifying, and validating AI systems.
      - Improving accuracy, validity, and reliability of model performance, while controlling bias.
      - Increasing the interpretability and privacy of learned models.
      - Reducing the vulnerability of models to families of adversarial attacks.
      - Advancing capabilities for assuring that model functionality aligns with societal values and obeys safety guarantees.